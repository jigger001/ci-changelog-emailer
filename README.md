# CI Emailer


## Development
To get started, ensure you are running Node.js 8+. Install dependencies with:

```bash
npm install
```

The `email-template` sub-directory has its own README instructions to modify/build the email template.

## Deployment
You may need to add `--profile` and `--region` to the `aws lambda` command depending on your local setup.

```bash
npm ci && \
zip -rq lambda.zip * -x '.git' && \
aws lambda update-function-code \
  --function-name 'ci-changelog-emailer' \
  --zip-file 'fileb://lambda.zip'
```
