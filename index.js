const fs = require('fs');
const sgMail = require('@sendgrid/mail');
const template = require('es6-template-strings');
// const email = fs.readFileSync('email-template/dist/achievement.html', 'utf8');

var async = require('async');
var AWS = require('aws-sdk');
var util = require('util');
var s3 = new AWS.S3();

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

exports.handler = async function(event, context) {
    // Read options from the event.
    var srcBucket = event.Records[0].s3.bucket.name;
    var buildFile = event.Records[0].s3.object.key;
    var event = event.Records[0];
    var buildPath = buildFile.substr(0, buildFile.lastIndexOf('/')+1);
    var s3Path = 'https://s3.console.aws.amazon.com/s3/buckets/' + srcBucket + '/' + buildPath;
    
    console.log("BuildPath", s3Path)
    console.log("Reading options from event:\n", util.inspect(event, {depth: 5}));
    console.log('Received event:', JSON.stringify(event, null, 2));

    const msg = {
        to: 'jigger.fantonial@dragontailsystems.com', 
        from: 'ci@dragontailsystem.com', 
        subject: `CI build: ${buildPath} is now ready!`,
        html: `<strong><a href=${s3Path}>${s3Path}</a></strong>`
    };

    await sgMail.send(msg);

    return {
        statusCode: 200,
        body: 'OK'
    };
};
